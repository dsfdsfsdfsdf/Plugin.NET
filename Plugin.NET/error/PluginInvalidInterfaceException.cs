﻿using System;

namespace PluginNET.error
{

    /// <summary>
    /// 实例化 PluginManager 的泛型参数不是由abstract修饰的类时会引发此异常
    /// </summary>
    [Serializable]
    public class PluginInvalidInterfaceException : Exception
    {
        /// <summary>
        /// 实例化 PluginManager 的泛型参数不是由abstract修饰的类时会引发此异常
        /// </summary>
        public PluginInvalidInterfaceException() : base("实例化 PluginManager 的泛型参数不是由abstract修饰的类")
        {

        }
    }
}
